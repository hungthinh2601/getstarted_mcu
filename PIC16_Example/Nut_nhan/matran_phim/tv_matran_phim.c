const unsigned char maquetcot[] = {0xfe,0xfd,0xfb,0xf7};
unsigned int quet_mt_phim()
{
   signed int8 maphim, hang, cot;
   maphim = hang = 0xff;
   for(cot=0;cot<4;cot++)
   {
      output_b(maquetcot[cot]);
      if(!input(pin_b4))
      {
         hang=0;
         break;
      }
      else if(!input(pin_b5))
      {
         hang = 1;
         break;
      }
      else if(!input(pin_b6))
      {
         hang = 2;
         break;
      }
      else if(!input(pin_b7))
      {
         hang = 3;
         break;
      }
   }
   if(hang!=0xff)    maphim = cot*4 + hang;
   return(maphim);
   
}

unsigned int key_4x4()
{
   unsigned int8 mpt1, mpt2;
   mpt1 = quet_mt_phim();   //6
   if(mpt1!=0xff)
   {
      delay_ms(10);
      mpt1=quet_mt_phim();  //6
      do
      {
         mpt2=quet_mt_phim();  //6 khi nha thi mpt2 # 6
      }
      while(mpt2==mpt1);  //co nha phim chua 
   }
   return(mpt1);
}


unsigned int key_4x4_dw()
{
   unsigned int8 mpt1=0, mpt2=0;
   mpt1=quet_mt_phim();  //6
   if(mpt1!=0xff)   //co nhan phim    
   {
      if(mpt1!=mpt2)   
      {
            return(mpt1);  //tao phim moi
            mpt2 = mpt1;
      }
      else
      {
            delay_ms(1);
            mpt1=quet_mt_phim();
            do
            {
               mpt2=quet_mt_phim();  //6
            }
            while(mpt2!=mpt1);  //co nhan phim chua
            return(mpt1);  // tra ve ma phim moi
            mpt2=mpt1;
      }
   }
   else
   {
         return(mpt1);// kiem tra chua co nhan phim
         mpt2=mpt1;
   }
}
