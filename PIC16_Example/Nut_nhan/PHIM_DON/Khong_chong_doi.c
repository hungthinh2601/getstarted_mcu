#INCLUDE <16F887.H>                             //khai b�o thu vien
#FUSES NOWDT, PUT, HS, NOPROTECT, NOLVP
#USE DELAY(CLOCK=20M)                           //ham delay

#DEFINE ON    PIN_B6                            //c�c dinh nghia nut nhan
#DEFINE OFF   PIN_B7
#DEFINE INV   PIN_B5

UNSIGNED INT8 X=0;  
unsigned int16   I,J,K,L ;//khai bao bien


void DELAY_TUY_Y(UNSIGNED INT8 DL1)          //chuong trinh con
{
   UNSIGNED INT8 DL;
   FOR(DL=0;DL<DL1;DL++)
   DELAY_MS(10);
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
VOID CHOP_TAT(UNSIGNED INT8 DL)
{
           //TAT LED
   OUTPUT_D(0X00);
   DELAY_TUY_Y(DL);
   
          //SANG LED
   OUTPUT_D(0XFF);
   DELAY_TUY_Y(DL);
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
VOID SANG_DAN(UNSIGNED INT8 DL)
{
      
      IF(J<17)
      {
         K=(K<<1)|1;
         OUTPUT_D(K);
         DELAY_TUY_Y(DL);
      }
      ELSE IF(J<33)
      {
         K=(K<<1);
         OUTPUT_D(K);  
         DELAY_TUY_Y(DL);
      }
      ELSE
      {
         J=0;
         K=0;
      }
      J++;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
VOID CHONG_DOI_INV()                         //chong doi
{  
   IF(!INPUT(INV))                     //kiem tra co nhan phim khong                     
   {
      DELAY_MS(20);                    //lam tre
      IF(!INPUT(INV))                  //kiem tra co nhan phim khong
      {
         X = ~X;                          //xu ly chuc nang phim
         OUTPUT_D(X);
      }
     // DO{}
      WHILE(!INPUT(INV));              //da nha phim chua
   }
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
VOID CHONG_DOI_CHON()                         //chong doi
{  
   IF(!INPUT(INV))                     //kiem tra co nhan phim khong                     
   {
      DELAY_MS(20);                    //lam tre
      IF(!INPUT(INV))                  //kiem tra co nhan phim khong
      {
         L = L + 1;                          //xu ly chuc nang phim
         IF(L==3)       L=0;
      }
     // DO{}
      WHILE(!INPUT(INV));              //da nha phim chua
   }
}
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
VOID MAIN()                         //chuong trinh chinh
{
   
   SET_TRIS_B(0XFF);
   SET_TRIS_D(0X00);
   X=0;
   OUTPUT_D(X);
   
   WHILE(TRUE)
   {
//!      WHILE(INPUT(ON));  //CO NHAN PHIM KHONG          
//!      X=0XFF;    OUTPUT_D(X);    //xu ly 
//!      DO
//!      {
//!         CHONG_DOI_INV(); 
//!      }
//!      WHILE(INPUT(OFF));
//!      OUTPUT_D(0X00);

            CHONG_DOI_CHON();
            IF(L==1)  CHOP_TAT(10);
            ELSE  SANG_DAN(10);
          
      
   }
}
