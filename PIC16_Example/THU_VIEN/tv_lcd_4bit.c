//#include <stddef.h>

#define LCD_RS          PIN_D0
#define LCD_EN          PIN_D1

#define LCD_D4          PIN_D7
#define LCD_D5          PIN_D6
#define LCD_D6          PIN_D5
#define LCD_D7          PIN_D4

// misc display defines-
#define Line_1          0x80
#define Line_2          0xC0
#define Clear_Scr       0x01

// prototype statements
#separate void LCD_Init ( void );// ham khoi tao LCD
#separate void LCD_SetPosition ( unsigned int cX );//Thiet lap vi tri con tro
#separate void LCD_DATA( unsigned int cX );// Ham viet1kitu/1chuoi len LCD
#separate void LCD_COMMAND ( unsigned int cX) ;// Ham gui lenh len LCD
#separate void LCD_PulseEnable ( void );// Xung kich hoat
#separate void LCD_SetData ( unsigned int cX );// Dat du lieu len chan Data
// D/n Cong
#use standard_io ( B )
#use standard_io (A)

UNSIGNED INT8 H,T;

//khoi tao LCD**********************************************
#separate void LCD_Init ( void )
    {

    LCD_SetData ( 0x00 );
    delay_ms(200);       /* wait enough time after Vdd rise >> 15ms */
    output_low ( LCD_RS );// che do gui lenh
    LCD_SetData ( 0x03 );   /* init with specific nibbles to start 4-bit mode */
    LCD_PulseEnable();
    LCD_PulseEnable();
    LCD_PulseEnable();
    LCD_SetData ( 0x02 );   /* set 4-bit interface */
    LCD_PulseEnable();      /* send dual nibbles hereafter, MSN first */
    LCD_COMMAND ( 0x2C );    /* function set (all lines, 5x7 characters) */
    LCD_COMMAND ( 0X0C);    /* display ON, cursor off, no blink */
    LCD_COMMAND ( 0x06 );    /* entry mode set, increment & scroll left */
    LCD_COMMAND ( 0x01 );    /* clear display */
    }

#separate void LCD_SetPosition ( unsigned int cX )
    {
    /* this subroutine works specifically for 4-bit Port A */
    LCD_SetData ( swap ( cX ) | 0x08 );
    LCD_PulseEnable();
    LCD_SetData ( swap ( cX ) );
    LCD_PulseEnable();
    }

#separate void LCD_DATA ( unsigned int cX )
    {
    /* this subroutine works specifically for 4-bit Port A */
        output_high ( LCD_RS );
        LCD_COMMAND( cX );
        output_low ( LCD_RS );
    }

#separate void LCD_COMMAND( unsigned int cX )
    {
    /* this subroutine works specifically for 4-bit Port A */
    LCD_SetData ( swap ( cX ) );     /* send high nibble */
    LCD_PulseEnable();
    LCD_SetData ( swap ( cX ) );     /* send low nibble */
    LCD_PulseEnable();
    }
#separate void LCD_PulseEnable ( void )
    {
    output_high ( LCD_EN );
    delay_us ( 3 );         // was 10
    output_low ( LCD_EN );
    delay_ms ( 3 );         // was 5
    }

#separate void LCD_SetData ( unsigned int cX )
    {
    output_bit ( LCD_D4, cX & 0x01 );
    output_bit ( LCD_D5, cX & 0x02 );
    output_bit ( LCD_D6, cX & 0x04 );
    output_bit ( LCD_D7, cX & 0x08 );
    }
    
//XXXXXXXXXXXXXXXXXXXXXXXXX FONT CHU LON CUA MA 8 DOAN XXXXXXXXXXXXXXXXXXXXXXXXXXX
CONST UNSIGNED CHAR LCD_SO_X[13][6] = {
                    0,1,2,5,3,4,                   //SO 0
                    1,2,32,3,7,3,                  //SO 1
                    6,6,2,5,3,3,                   //SO 2
                    6,6,2,3,3,4,                   //SO 3
                    7,3,7,32,32,7,                 //SO 4
                    7,6,6,3,3,4,                   //SO 5
                    0,6,6,5,3,4,                   //SO 6
                    1,1,7,32,32,7,                 //SO 7
                    0,6,2,5,3,4,                   //SO 8
                    0,6,2,3,3,4,                   //SO 9
                    32,32,32,32,32,32,
                    0,1,1,5,3,3,
                    0,6,2,32,32,32};
                    
                    
CONST UNSIGNED CHAR LCD_MA_8DOAN[] = {
   0x07,0x0F,0x1F,0x1F,0x1F,0x1F,0x1F,0x1F,  //DOAN F - 0
   0x1F,0x1F,0x1F,0X00,0X00,0X00,0X00,0X00,  //DOAN A - 1
   0x1C,0x1E,0x1F,0x1F,0x1F,0x1F,0x1F,0x1F,  //DOAN B - 2
   0X00,0X00,0X00,0X00,0X00,0x1F,0x1F,0x1F,  //DOAN D - 3
   0x1F,0x1F,0x1F,0x1F,0x1F,0x1F,0x1E,0x1C,  //DOAN C - 4
   0x1F,0x1F,0x1F,0x1F,0x1F,0x1F,0x0F,0x07,  //DOAN E - 5
   0x1F,0x1F,0x1F,0X00,0X00,0X00,0x1F,0x1F,  //DOAN G+D-6 
   0x1F,0x1F,0x1F,0x1F,0x1F,0x1F,0x1F,0x1F}; //DOAN I  -7
   
VOID LCD_GOTO_XY(SIGNED INT8 X, SIGNED INT8 Y)
{ 
   CONST UNSIGNED INT8 LCD_VITRI[]={0x80,0xC0,0x94,0xD4};
   LCD_COMMAND(LCD_VITRI[X]+Y);
}

VOID XXX()
{ 
   SIGNED INT8 X;
   X= LCD_SO_X[0][0];
   X= LCD_MA_8DOAN[0];
}

//XXXXXXXXXXXXXXXXXXXXXXXX HIEN THI CHU HAY SO LON CHIEM 6 O XXXXXXXXXXXXXXXXXXXXXXXXXXXX
VOID HIENTHI_FONT_LON(SIGNED INT8 SO, SIGNED INT8 X1,SIGNED INT8 Y1)
{
   LCD_GOTO_XY(X1,Y1);
   FOR(H=0;H<6;H++)
   {
      IF(H==3)       LCD_GOTO_XY(X1+1,Y1);
      LCD_DATA(LCD_SO_X[SO][H]);
   }
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
VOID LAY_MA_8DOAN()
{
   LCD_COMMAND(0X40);      DELAY_US(20);
   FOR(T=0;T<64;T++)
   {
      LCD_DATA(LCD_MA_8DOAN[T]);
   }
}
