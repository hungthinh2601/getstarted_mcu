#INCLUDE <16F887.H>                             //khai b�o thu vien
#fuses hs, put,noprotect,nolvp, nowdt
#use delay(clock = 20M)

VOID CHOP_TAT()
{
   OUTPUT_C(0X00);            //TAT LED
   OUTPUT_D(0X00);
   DELAY_MS(500);
    OUTPUT_C(0XFF);           //SANG LED
   OUTPUT_D(0XFF);
   DELAY_MS(500);
}

void main()                //chuong trinh chinh
{
   set_tris_c(0x00);       
   set_tris_d(0x00);

   while(true)
   {
        CHOP_TAT();
      
   }
}
