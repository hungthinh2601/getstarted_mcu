#INCLUDE <16F887.H>
#DEVICE ADC = 10
#FUSES NOWDT, PUT, HS, NOPROTECT, NOLVP
#USE DELAY(CLOCK=20M)
#INCLUDE <E:\Youtube\Pic16F887\THU_VIEN\TV_LCD_16X2.c>

unsigned INT8 gio,phut,giay,sao,BDT;
UNSIGNED INT8 I;
UNSIGNED INT16 KQ0;


#int_timer1
VOID INTERRUPT_TIMER1()
{
   SET_TIMER1(59286);
   BDT++;
}

void hienthi()
{
      lcd_command(0x80);
      lcd_data("NHIET DO:  ");
      LCD_COMMAND(0X8C);
      LCD_DATA(KQ0/10 + 0X30);
      LCD_DATA(KQ0%10 + 0X30);
      LCD_DATA("oC");
      
      lcd_command(0xC0);
      lcd_data("DHTT:");
      lcd_command(0xc5);
      lcd_data(gio/10 + 0x30);
      lcd_data(gio%10 + 0x30);
      lcd_data(":");
      lcd_data(phut/10 + 0x30);
      lcd_data(phut%10 + 0x30);
      lcd_data(":");
      lcd_data(giay/10 + 0x30);
      lcd_data(giay%10 + 0x30);
      lcd_data(":");
      lcd_data(sao/10 + 0x30);
      lcd_data(sao%10 + 0x30);
   
}

VOID DOC_LM350()
{
      KQ0=0;
      FOR(I=0;I<100;I++)
      {
         KQ0 = KQ0 + READ_ADC();
         DELAY_MS(1);
      }
      KQ0 = KQ0/2.046/100;
}

VOID MAIN()
{
   SET_TRIS_D(0X00);
   SET_TRIS_A(0X01);
   
   lcd_setup();
   GIAY=0;
   PHUT=0;
   GIO=0;
   SAO=0;
   BDT=0;
   HIENTHI();
   SETUP_TIMER_1(T1_INTERNAL|T1_DIV_BY_8);
   SET_TIMER1(59286);
   
   SETUP_ADC(ADC_CLOCK_DIV_32);
   SETUP_ADC_PORTS(SAN0);
   SET_ADC_CHANNEL(0);
   
   ENABLE_INTERRUPTS(GLOBAL);
   ENABLE_INTERRUPTS(INT_TIMER1);
  
   
   WHILE(TRUE)
   {
         IF(BDT>=4)
         {  
            BDT=0;
            SAO++;
            IF(SAO==25)
            {  
               SAO=0;
               GIAY++; 
               IF(GIAY==60) 
               {
                  GIAY=0;
                  PHUT++;
                  IF(PHUT==60)
                     {
                        PHUT=0;
                        GIO++;
                        IF(GIO==24)       GIO=0;
                     }
               }
            }
            
         }
         ELSE 
         {
           
           DOC_LM350();
           HIENTHI();
         }
   }
}
