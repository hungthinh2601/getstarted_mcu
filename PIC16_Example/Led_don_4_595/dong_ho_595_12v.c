#INCLUDE <16F887.H>
#DEVICE  ADC=10
#FUSES  NOLVP, HS, PUT, NOPROTECT, NOWDT
#USE DELAY(CLOCK=20M)

#INCLUDE <E:\Youtube\Pic16F887\THU_VIEN\tv_4led_7doan.c>
#INCLUDE <E:\Youtube\Pic16F887\THU_VIEN\TV_DS13B07.c>

UNSIGNED CHAR CGIAY,DVGIAY;
VOID GIAIMA()
{
   CGIAY = GIAY_DS13/16;
   DVGIAY = GIAY_DS13%16;
}

VOID HIENTHI()
{
   XUAT_4LED_7DOAN_4SO_NOT(0XFF,0XFF,CGIAY,DVGIAY);
}

VOID MAIN()
{
   SET_TRIS_D(0X00);
   IF(MA_DS13!=MA_DS)
   {
      THIET_LAP_THOI_GIAN_HIEN_TAI();
      NAP_THOI_GIAN_HTAI_VAO_DS13B07();
   }
   DOC_THOI_GIAN_TU_REALTIME();
   GIAYTAM = GIAY_DS13;
   XUAT_4LED_7DOAN_4SO(0XFF,0XFF,0XFF,0XFF);
   WHILE(TRUE)
   {
      DOC_THOI_GIAN_TU_REALTIME();
      IF(GIAYTAM!=GIAY_DS13)
      {
         GIAYTAM=GIAY_DS13;
         GIAIMA();
         HIENTHI();
      }
     
   }
}


