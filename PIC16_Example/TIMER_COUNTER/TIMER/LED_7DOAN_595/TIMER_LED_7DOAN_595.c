#INCLUDE <16F887.H>
#FUSES NOWDT, PUT, HS, NOPROTECT, NOLVP
#USE DELAY(CLOCK=20M)
#INCLUDE <E:\Youtube\Pic16F887\THU_VIEN\tv_4led_7doan.c>

#BIT  TMR1IF = 0X0C.0   //bien co tran


UNSIGNED INT8  GIAY,BDT,PHUT;

VOID MAIN()
{
   SET_TRIS_D(0X00);
   GIAY=0;
   PHUT=0;
   BDT=0;
   XUAT_4LED_7DOAN_4SO(0XFF,0XFF,0XFF,0XFF);
   SETUP_TIMER_1(T1_INTERNAL|T1_DIV_BY_8);
   SET_TIMER1(3036);
   
   WHILE(TRUE)
   {
      IF(TMR1IF==1)
      {
         TMR1IF=0;
         SET_TIMER1(3036);
         BDT++;
         XUAT_4LED_7DOAN_4SO(MA7DOAN[PHUT/10],MA7DOAN[PHUT%10],MA7DOAN[GIAY/10],MA7DOAN[GIAY%10]);
         IF(BDT==10)
         {  
            
            GIAY++;
            IF(GIAY==60)
            {
               GIAY=0;
               PHUT++;
                  IF(PHUT==60)
                  {
                     PHUT=0;
                  }
               
            }
            BDT=0;
         }
      }
   }
}
