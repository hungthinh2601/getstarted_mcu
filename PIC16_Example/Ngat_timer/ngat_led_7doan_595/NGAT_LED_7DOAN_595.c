#INCLUDE <16F887.H>
#FUSES NOWDT, PUT, HS, NOPROTECT, NOLVP
#USE DELAY(CLOCK=20M)
#INCLUDE <E:\Youtube\Pic16F887\THU_VIEN\tv_4led_7doan.c>

UNSIGNED INT8  GIAY, PHUT,BDT;


#int_timer1
VOID INTERRUPT_TIMER1()
{
   SET_TIMER1(3036);
   BDT++;
}


VOID MAIN()
{
   SET_TRIS_D(0X00);
   SET_TRIS_C(0X00);
   
   SETUP_TIMER_1(T1_INTERNAL|T1_DIV_BY_8);
   SET_TIMER1(3036);
   
   ENABLE_INTERRUPTS(GLOBAL);
   ENABLE_INTERRUPTS(INT_TIMER1);
   GIAY=0;
   PHUT=0;
   BDT=0;
   
   WHILE(TRUE)
   {  
        IF(BDT>=10)
        {
             BDT=0;
             GIAY++;
             IF(GIAY==60)
             {
               GIAY=0;
               PHUT++;
               IF(PHUT==60)      PHUT=0;
              
             }
        }
        ELSE 
        XUAT_4LED_7DOAN_4SO(MA7DOAN[PHUT/10],MA7DOAN[PHUT%10],MA7DOAN[GIAY/10],MA7DOAN[GIAY%10]);
   }
   
}
