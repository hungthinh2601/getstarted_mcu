#include <TimerOne.h> // khai báo thư viện timer 1

void setup()
{
  pinMode(6,OUTPUT);
  pinMode(12,OUTPUT);
  Timer1.initialize(1000000); // khởi tạo timer 1 đến 1 giây
  Timer1.attachInterrupt(Blink); // khai báo ngắt timer 1

  // digitalWrite (12, HIGH); 
  // digitalWrite (6, LOW); 
}
void loop()
{
  digitalWrite (12, HIGH); 
}
void Blink()
{
  digitalWrite(6, HIGH);
}
