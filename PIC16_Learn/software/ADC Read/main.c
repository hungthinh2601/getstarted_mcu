#include <main.h>

char chuoi[17] = "ADC read"; 

void main()
{
   int16 Adc; 
   
   lcd_init(); 
   lcd_clear(); 
   
   setup_adc(ADC_CLOCK_INTERNAL); 
   setup_adc_ports(AN0); 
   SET_ADC_CHANNEL(0); 

   while(TRUE)
   {
      Adc = read_adc(); 
      
      lcd_gotoxy(1,1); 
      sprintf (chuoi, "ADC=%04ld", Adc);
      lcd_puts(chuoi); 
      delay_ms (500); 
   }

}
