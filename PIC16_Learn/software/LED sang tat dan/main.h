#include <16F877A.h>
#device ADC=16

#FUSES HS
#FUSES NOWDT                    //No Watch Dog Timer
#FUSES NOBROWNOUT               //No brownout reset
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O

#use delay(crystal=20000000)

// Khai bao LED sang dan 
#define LED0(x)         output_bit(PIN_B0, x); 
#define LED1(x)         output_bit(PIN_B1, x);
#define LED2(x)         output_bit(PIN_B2, x);
#define LED3(x)         output_bit(PIN_B3, x);
#define LED4(x)         output_bit(PIN_B4, x);
#define LED5(x)         output_bit(PIN_B5, x);
#define LED6(x)         output_bit(PIN_B6, x);
#define LED7(x)         output_bit(PIN_B7, x);

