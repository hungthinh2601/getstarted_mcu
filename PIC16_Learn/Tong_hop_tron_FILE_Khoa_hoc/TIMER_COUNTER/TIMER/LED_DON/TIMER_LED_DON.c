#INCLUDE <16F887.H>
#FUSES NOWDT, PUT, HS, NOPROTECT, NOLVP
#USE DELAY(CLOCK=20M)

#BIT  TMR1IF = 0X0C.0      //co tran nam o bit thu 0 trong thanh ghi PIR1 co dia chi la 0x0C

UNSIGNED INT8  X,BDT;

VOID MAIN()
{
   SET_TRIS_D(0X00);
   X=0;
   BDT=0;
   OUTPUT_D(X);
   SETUP_TIMER_1(T1_INTERNAL|T1_DIV_BY_8);
   SET_TIMER1(3036);
   
   WHILE(TRUE)
   {
      IF(TMR1IF==1)
      {
         TMR1IF=0;
         SET_TIMER1(3036);
         BDT++;
         OUTPUT_D(X);
         IF(BDT==10)
         {
            X=(X<<1)|0x01;
            BDT=0;
         }
      }
   }
}
