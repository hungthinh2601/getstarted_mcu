#INCLUDE <16F887.H>
#FUSES NOWDT, PUT, HS, NOPROTECT, NOLVP
#USE DELAY(CLOCK=20M)


UNSIGNED INT8 T0,DV,CHUC,TRAM;
UNSIGNED INT16 L;

CONST UNSIGNED CHAR MA7DOAN[16]= {0XC0,0XF9,0xA4,0XB0,0X99,0X92,0X82,0XF8,
0X80,0X90,0x88, 0x83, 0xC6, 0xA1, 0x86, 0x8E};

VOID HIENTHI()
{  
               
     //  FOR(L=0;L<10;L++)
       //  {
                OUTPUT_B(TRAM);    OUTPUT_LOW(PIN_D5);     DELAY_MS(1);      OUTPUT_HIGH(PIN_D5);
                OUTPUT_B(CHUC);    OUTPUT_LOW(PIN_D6);     DELAY_MS(1);      OUTPUT_HIGH(PIN_D6);
                OUTPUT_B(DV);      OUTPUT_LOW(PIN_D7);     DELAY_MS(1);      OUTPUT_HIGH(PIN_D7);
        // }       
         
}

VOID MAIN()
{
   SET_TRIS_D(0X00);  
   SET_TRIS_B(0X00); 
   
   SETUP_TIMER_0(T0_EXT_L_TO_H|T0_DIV_1);
   SET_TIMER0(0);
   
   WHILE(TRUE)
   {
      T0 = GET_TIMER0();
      DV=MA7DOAN[T0%10];      CHUC = MA7DOAN[T0/10%10];     TRAM = MA7DOAN[T0/100];
     
     IF(TRAM==0XC0)
     {
         TRAM=0XFF;
         IF(CHUC==0XC0)
         {
            CHUC = 0XFF;
         }
     }
     HIENTHI();
      
      IF(T0==120)  SET_TIMER0(1);
   }
}
