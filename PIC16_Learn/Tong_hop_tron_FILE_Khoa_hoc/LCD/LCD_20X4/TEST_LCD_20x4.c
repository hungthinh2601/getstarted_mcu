#INCLUDE <16F887.H>
#DEVICE  ADC=10
#FUSES NOWDT, PUT, HS, NOPROTECT, NOLVP
#USE DELAY(CLOCK=20M)
#INCLUDE <E:\Youtube\Pic16F887\THU_VIEN\TV_LCD_20x4.c>

#USE I2C(MASTER,SLOW,SDA=PIN_C4,SCL=PIN_C3)
#USE FAST_IO(B)
#use FAST_IO(D)
#use FAST_IO(E)

#DEFINE BUZZER   PIN_C0

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
#DEFINE MOD PIN_E0
#DEFINE UP  PIN_E1
#DEFINE DW  PIN_E2

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
#DEFINE   GIAY_HTAI      0X55
#DEFINE   PHUT_HTAI      0X00
#DEFINE   GIO_HTAI       0X19
#DEFINE   THU_HTAI       0x02
#DEFINE   NGAY_HTAI      0X16
#DEFINE   THANG_HTAI     0X06
#DEFINE   NAM_HTAI       0X13      
#DEFINE   MA_DS          0X98

#DEFINE  ADDR_WR_13B07   0xD0
#DEFINE  ADDR_RD_13B07   0xD1
#DEFINE  ADDR_MEM        0x00


// MANG HAI CHIEU GIANH CHO NGAY AM LICH
const unsigned char ALdauthangDL[16][12] = {
   {22,23,23,24,25,26,27,29, 1, 1, 2, 3},//2016
   { 4, 5, 4, 5, 6, 7, 8,10,11,12,13,14},//2017
   {15,16,14,16,16,18,18,20,22,22,24,25},//2018
   {26,27,25,27,27,28,29, 1, 3, 3, 5, 6},//2019
   { 7, 8, 8, 9, 9,10,11,12,14,15,16,17},//2020

   {19,20,18,20,20,21,22,23,25,25,27,27},//2021
   {29, 1,29, 1, 1, 3, 3, 4, 6, 6, 8, 8},//2022
   {10,11,20,11,12,14,14,15,17,17,18,19},//2023
   {20,22,21,23,23,25,26,27,29,29, 1, 1},//2024
   { 2, 4, 2, 4, 4, 6, 7, 8,10,10,12,12},//2025
   {13,14,13,14,15,16,17,19,20,21,23,23},//2026
   {24,25,24,25,25,27,27,29, 1, 2, 4, 4},//2027   
   { 5, 7, 6, 7, 7, 9, 9,11,13,13,15,16},//2028
   {17,18,17,18,18,20,20,22,23,24,25,26},//2029
   {28,29,28,29,29, 1, 1, 3, 4, 5, 6, 7}//2030
};

const unsigned char thangALdauthangDL[16][12] = {
   {11,12, 1, 2, 3, 4, 5, 6, 8, 9,10,11},//2016
   {12, 1, 2, 3, 4, 5, 6, 6, 7, 8, 9,10},//2017
   {11,12, 1, 2, 3, 4, 5, 6, 7, 8, 9,10},//2018
   {11,12, 1, 2, 3, 4, 5, 7, 8, 9,10,11},//2019
   {12, 1, 2, 3, 4, 4, 5, 6, 7, 8, 9,10},//2020

   {11,12, 1, 2, 3, 4, 5, 6, 7, 8, 9,10},//2021
   {11, 1, 1, 3, 4, 5, 6, 7, 8, 9,10,11},//2022
   {12, 1, 2, 2, 3, 4, 5, 6, 7, 8, 9,10},//2023
   {11,12, 1, 2, 3, 4, 5, 6, 7, 8,10,11},//2024
   {12, 1, 2, 3, 4, 5, 6, 6, 7, 8, 9,10},//2025
   {11,12, 1, 2, 3, 4, 5, 6, 7, 8, 9,10},//2026
   {11,12, 1, 2, 3, 4, 5, 6, 8, 9,10,11},//2027
   {12, 1, 2, 3, 4, 5, 5, 6, 7, 8, 9,10},//2028
   {11,12, 1, 2, 3, 4, 5, 6, 7, 8, 9,10},//2029
   {11,12, 1, 2, 3, 5, 6, 7, 8, 9,10,11}//2030
};

const unsigned char DLdauthangAL[16][12] = {
   {10, 8, 9, 7, 7, 5, 4, 3, 1,31,29,29},//2016
   {28,26,28,26,26,24,23,22,20,20,18,18},//2017
   {17,16,17,16,15,14,13,11,10, 9, 7, 7},//2018
   { 6, 5, 6, 5, 5, 3, 3,30,29,28,26,26},//2019
   {25,23,24,23,23,21,21,19,17,17,15,14},//2020

   {13,12,13,12,12,10,10, 8, 7, 6, 5, 4},//2021
   { 3,29, 3,31,30,29,29,27,26,25,24,23},//2022
   {22,20,22,20,19,18,18,16,15,15,13,13},//2023
   {11,10,10, 9, 8, 6, 6, 4, 3, 3,31,31},//2024
   {29,28,29,28,27,25,25,23,22,21,20,20},//2025
   {19,17,19,17,17,15,14,13,11,10, 9, 9},//2026
   { 8, 6, 8, 7, 6, 5, 4, 1,30,29,28,28},//2027
   {26,25,26,25,24,23,22,20,19,18,16,16},//2028
   {15,23,15,14,13,12,11,10, 8, 8, 6, 5},//2029
   { 4, 2, 4,31, 2,30,30,29,27,27,25,25}//2030
};

const unsigned char thangDLdauthangAL[16][12] = {
   {12, 1, 2, 3, 4, 5, 6, 7, 8,10,11,12},//2016
   { 1, 2, 3, 4, 5, 6, 6, 7, 8, 9,10,11},//2017
   {12, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11},//2018
   {12, 1, 2, 3, 4, 5, 6, 8, 9,10,11,12},//2019
   { 1, 2, 3, 4, 4, 5, 6, 7, 8, 9,10,11},//2020
   {12, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11},//2021
   {12, 1, 2, 3, 5, 6, 7, 8, 9,10,11,12},//2022
   { 1, 2, 2, 3, 4, 5, 6, 7, 8, 9,10,11},//2023
   {12, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,12},//2024
   { 1, 2, 3, 4, 5, 6, 6, 7, 8, 9,10,11},//2025
   {12, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11},//2026
   {12, 1, 2, 3, 4, 5, 6, 7, 9,10,11,12},//2027
   { 1, 2, 3, 4, 5, 5, 6, 7, 8, 9,10,11},//2028
   {12, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11},//2029
   {12, 1, 2, 3, 4, 5, 7, 8, 9,10,11,12} //2030
};

// MANG NAY TOI NAM 2030 MUON THEM THI TANG GIA TRI L� OK.
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//XXXXXXXXXXXXXXXXXX KHAI BAO BIEN XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX//
UNSIGNED  CHAR    NAM_DS13,THANG_DS13,NGAY_DS13,THU_DS13,GIO_DS13,
                  PHUT_DS13,GIAY_DS13,MA_DS13,CONTROL_DS13;       // BIEN CHO THOI GIAN LAY TU CON DS CHUYEN QUA VA CHUYEN VAO DS.
UNSIGNED INT8 I,DEM;
 UNSIGNED INT16 J, KQADC=0,K;                  
UNSIGNED CHAR NGAY_AM, THANG_AM;
UNSIGNED INT8 GIATRI_MOD;

CONST UNSIGNED CHAR MA7DOAN[10]= {0XC0,0XF9,0xA4,0XB0,0X99,0X92,0X82,0XF8,0X80,0X90};  // BANG MA 7 DOAN DE HIEN THI.
CONST UNSIGNED CHAR DAUDO[] = {0X0F,0X09,0X09,0X0F,0,0,0,0};
CHAR BCD_BIN(CHAR BCD)
{
  CHAR X,Y;

  X = (BCD>>4)*0x0A;
  Y = BCD<<4;
  Y = Y>>4;
  RETURN(X+Y);
}             


INT8 BIN_BCD(INT8 BIN)
{
  INT8 L, H;

  L= BIN;
  H = 0;

  WHILE(TRUE)
  {
    IF(L >= 10)
    {
      L -= 10;
      H += 0x10;
    }
    ELSE
    {
      H += L;
      BREAK;
    }
  }

  RETURN(H);
}

//XXXXXXXXXXXXXXXXXXXXXXXXXX THOI GIAN TU CON DS13B07 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX//
void THIET_LAP_THOI_GIAN_HIEN_TAI()
{   GIAY_DS13    =    GIAY_HTAI;   PHUT_DS13    =   PHUT_HTAI;
    GIO_DS13     =    GIO_HTAI;    
    NGAY_DS13    =    NGAY_HTAI;   THANG_DS13   =   THANG_HTAI;
    NAM_DS13     =    NAM_HTAI;
    CONTROL_DS13  =  0X90;         MA_DS13   =   MA_DS;
}

void NAP_THOI_GIAN_HTAI_VAO_DS13B07()
{   I2C_START();
    I2C_WRITE(ADDR_WR_13B07);
    I2C_WRITE(0X00);   
    I2C_WRITE(BIN_BCD(GIAY_DS13));   I2C_WRITE(BIN_BCD(PHUT_DS13));
    I2C_WRITE(BIN_BCD(GIO_DS13));    I2C_WRITE(BIN_BCD(THU_DS13));
    I2C_WRITE(BIN_BCD(NGAY_DS13));   I2C_WRITE(BIN_BCD(THANG_DS13));
    I2C_WRITE(BIN_BCD(NAM_DS13));    I2C_WRITE(CONTROL_DS13);
    I2C_WRITE(MA_DS13);
    I2C_STOP();
}

void DOC_THOI_GIAN_TU_REALTIME()
{
I2C_START();
I2C_WRITE(ADDR_WR_13B07);
I2C_WRITE(ADDR_MEM);
I2C_START();
I2C_WRITE(ADDR_RD_13B07);
GIAY_DS13   = BCD_BIN(I2C_READ());
PHUT_DS13   = BCD_BIN(I2C_READ());
GIO_DS13    = BCD_BIN(I2C_READ());
THU_DS13    = BCD_BIN(I2C_READ());
NGAY_DS13   = BCD_BIN(I2C_READ());
THANG_DS13  = BCD_BIN(I2C_READ());
NAM_DS13    = BCD_BIN(I2C_READ());
CONTROL_DS13   = I2C_READ();
MA_DS13     =    I2C_READ(0);   
I2C_STOP();
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX CHUYEN DUONG LICH SANG AM LICH XXXXXXXXXXXXXXXXXXXXXXXXXX

VOID LICH_VIETNAM(UNSIGNED CHAR NGAY, THANG, NAM)
{
   UNSIGNED CHAR LUU;
   UNSIGNED CHAR DA,DB;
   UNSIGNED CHAR LUU_THANG;
   IF(NAM<=30) //cho phep den nam 2030
     {
         DA = ALdauthangDL[NAM-16][THANG-1];
         DB = DLdauthangAL[NAM-16][THANG-1];   
         LUU = DB-NGAY;
         IF(LUU<=0)
            {
               NGAY_AM=(NGAY-DB+1);
               THANG_AM  = thangDLdauthangAL[NAM-16][THANG-1];
            }
         
         ELSE
            {
               IF(LUU>31)
               {
                  NGAY_AM = (NGAY-DB+1);
                  THANG_AM = thangALdauthangDL[NAM-16][THANG];
               }
               ELSE
                  {
                     NGAY_AM = (NGAY+DA-1);
                     THANG_AM = thangALdauthangDL[NAM-16][THANG-1];
                  }
            }
         LUU_THANG = THANG_AM;
         
     }
}  



VOID HIENTHI_NHIET_DO()
{
  
   FOR(J=0;J<100;J++)
   {
      KQADC = KQADC+READ_ADC();  
      DELAY_MS(1);
   }
   
   KQADC = KQADC/2.046/100;
   
   
   IF(KQADC>40)
   {
      OUTPUT_HIGH(BUZZER);
   }
   
   ELSE
   {
      OUTPUT_LOW(BUZZER);
   }
   
   
   
}

VOID HIENTHI_THOI_GIAN()
{
   LCD_COMMAND(0X80);
   LCD_DATA(NGAY_DS13/10 + 0X30);
   LCD_DATA(NGAY_DS13%10 + 0X30);
   LCD_DATA("/");
   LCD_DATA(THANG_DS13/10 + 0X30);
   LCD_DATA(THANG_DS13%10 + 0X30);
   LCD_DATA("/20");
   LCD_DATA(NAM_DS13/10 + 0X30);
   LCD_DATA(NAM_DS13%10 + 0X30);
   
   LCD_COMMAND(0X8C);
   LCD_DATA(NGAY_AM/10 + 0X30);
   LCD_DATA(NGAY_AM%10 + 0X30);
   LCD_DATA("/");
   LCD_DATA(THANG_AM/10 + 0X30);
   LCD_DATA(THANG_AM%10 + 0X30);
   LCD_DATA(" AL");
   
   IF(THU_DS13==8)
   {
         LCD_COMMAND(0XE0);
         LCD_DATA("CHU NHAT");
         
   }
   
   ELSE
   {
         LCD_COMMAND(0XE3);
         LCD_DATA("THU:");
         LCD_DATA(THU_DS13%10 + 0X30);
   }
   
   LCD_COMMAND(0XD4);  
   LCD_DATA("NHIET DO:");
   LCD_DATA((KQADC/10)+0X30);
   LCD_DATA((KQADC%10) + 0X30);
   
   LCD_DATA(0XDF);
   LCD_DATA(0X43);
   
   HIENTHI_FONT_LON(GIO_DS13/10,1,0);
   HIENTHI_FONT_LON(GIO_DS13%10,1,3);
   LCD_COMMAND(0XC6);
   LCD_DATA(":");
   LCD_COMMAND(0X9A);
   LCD_DATA(":");
   HIENTHI_FONT_LON(PHUT_DS13/10,1,7);
   HIENTHI_FONT_LON(PHUT_DS13%10,1,10);
   LCD_COMMAND(0XCD);
   LCD_DATA(":");
   LCD_COMMAND(0XA1);
   LCD_DATA(":");
   HIENTHI_FONT_LON(GIAY_DS13/10,1,14);
   HIENTHI_FONT_LON(GIAY_DS13%10,1,17);
   
}

//XXXXXXXXXXXXXXXXXXXX NUT NHAN XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
VOID PHIM_MOD()
{
   IF(!INPUT(MOD))
   {
         DELAY_MS(20);
      IF(!INPUT(MOD))
      {
         IF(GIATRI_MOD<8)  GIATRI_MOD++;
         ELSE              GIATRI_MOD=0;
     
         WHILE(!INPUT(MOD));
      }
   }
}

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
 VOID PHIM_UP()
 {
   IF (!INPUT(UP))
   {
   DELAY_MS(20);
      IF (!INPUT(UP))
      {
       SWITCH (GIATRI_MOD)
       {
         CASE 1: IF (NGAY_DS13==31) NGAY_DS13=1;
                  ELSE
                  {
                     NGAY_DS13++;
                     OUTPUT_HIGH(BUZZER);
                  }
                  BIN_BCD(NGAY_DS13);
                  NAP_THOI_GIAN_HTAI_VAO_DS13B07();
                  BREAK;
         
         CASE 2: IF (THANG_DS13==12) THANG_DS13=1;
                  ELSE
                  {
                      THANG_DS13++;
                  }
                  BIN_BCD(THANG_DS13);
                  NAP_THOI_GIAN_HTAI_VAO_DS13B07();
                  BREAK;
         
         CASE 3: IF (NAM_DS13==99) NAM_DS13=0;
                  ELSE
                  {
                     NAM_DS13++;
                  }
                  BIN_BCD(NAM_DS13);
                  NAP_THOI_GIAN_HTAI_VAO_DS13B07();
                  BREAK;
         
         CASE 4: IF(THU_DS13==8)    THU_DS13=2;
                 ELSE
                 {
                     THU_DS13++;
                 }
                 BIN_BCD(THU_DS13);
                 NAP_THOI_GIAN_HTAI_VAO_DS13B07();
                 BREAK;
                 
         CASE 5: IF (GIO_DS13==23) GIO_DS13=0;
                  ELSE
                  {
                     GIO_DS13++;
                  }
                  BIN_BCD(GIO_DS13);
                  NAP_THOI_GIAN_HTAI_VAO_DS13B07();
                  BREAK;
         
         CASE 6: IF (PHUT_DS13==59) PHUT_DS13=0;
                  ELSE
                  {
                     PHUT_DS13++;
                  }
                  BIN_BCD(PHUT_DS13);
                  NAP_THOI_GIAN_HTAI_VAO_DS13B07();
                  BREAK;
         
         
         CASE 7: IF (GIAY_DS13==59) GIAY_DS13=0;
                  ELSE
                  {
                     GIAY_DS13++;
                  }
                  BIN_BCD(NAM_DS13);
                  NAP_THOI_GIAN_HTAI_VAO_DS13B07();
                  BREAK;
                  
         DEFAULT: BREAK;
       }
        DO{}
        WHILE(!INPUT(UP));
      }
   }
 }
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
VOID PHIM_DW()
 {
   IF (!INPUT(DW))
   {
   DELAY_MS(20);
      IF (!INPUT(DW))
      {
       SWITCH (GIATRI_MOD)
       {
         CASE 1: IF (NGAY_DS13==1) NGAY_DS13=31;
                  ELSE
                  {
                     NGAY_DS13--;
                  }
                  BIN_BCD(NGAY_DS13);
                  NAP_THOI_GIAN_HTAI_VAO_DS13B07();
                  BREAK;
         
         CASE 2: IF (THANG_DS13==1) THANG_DS13=12;
                  ELSE
                  {
                     THANG_DS13--;
                  }
                  BIN_BCD(THANG_DS13);
                  NAP_THOI_GIAN_HTAI_VAO_DS13B07();
                  BREAK;
         
         CASE 3: IF (NAM_DS13==99) NAM_DS13=0;
                  ELSE
                  {
                     NAM_DS13--;
                  }
                  BIN_BCD(NAM_DS13);
                  NAP_THOI_GIAN_HTAI_VAO_DS13B07();
                  BREAK;
         
         CASE 4: IF(THU_DS13==2)    THU_DS13=8;
                 ELSE
                 {
                     THU_DS13--;
                 }
                 BIN_BCD(THU_DS13);
                 NAP_THOI_GIAN_HTAI_VAO_DS13B07();
                 BREAK;
                 
         CASE 5: IF (GIO_DS13==0) GIO_DS13=23;
                  ELSE
                  {
                     GIO_DS13--;                  
                  }
                  BIN_BCD(GIO_DS13);
                  NAP_THOI_GIAN_HTAI_VAO_DS13B07();
                  BREAK;
         
         CASE 6: IF (PHUT_DS13==0) PHUT_DS13=59;
                  ELSE
                  {
                     PHUT_DS13--;                  
                  }
                  BIN_BCD(PHUT_DS13);
                  NAP_THOI_GIAN_HTAI_VAO_DS13B07();
                  BREAK;
         
         
         CASE 7: IF (GIAY_DS13==0) GIAY_DS13=59;
                  ELSE
                  {
                      GIAY_DS13--;                 
                  }
                  BIN_BCD(GIAY_DS13);
                  NAP_THOI_GIAN_HTAI_VAO_DS13B07();
                  BREAK;
         
         DEFAULT: BREAK;
       }
        DO{}
        WHILE(!INPUT(DW));
      }
   }
 } 

VOID NHAP_NHAY()
{
   SWITCH (GIATRI_MOD)
   {
      CASE 1: LCD_COMMAND(ADDR_LINE1);
              BREAK;

      CASE 2: LCD_COMMAND(0X83);
              BREAK;
      
      CASE 3: LCD_COMMAND(0X81);
              BREAK;
      
      CASE 4: LCD_COMMAND(0X81);
              BREAK;
      
      CASE 5: LCD_COMMAND(0X81);
              BREAK;
              
      CASE 6: LCD_COMMAND(0X81);
              BREAK;
   }
}
//XXXXXXXXXXXXXXXXXXXXXXXXXXX CHUONG TRINH CHINH XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
VOID MAIN()
{
   SET_TRIS_D(0X00);
   SET_TRIS_B(0X00);
   SET_TRIS_A(0X01);
   SETUP_ADC(ADC_CLOCK_DIV_32);
   SETUP_ADC_PORTS(SAN0);
   SET_ADC_CHANNEL(0);
   LCD_SETUP();
   
   DOC_THOI_GIAN_TU_REALTIME();
    LICH_VIETNAM(NGAY_DS13,THANG_DS13,NAM_DS13);

   IF (MA_DS13!=MA_DS)
   {
      THIET_LAP_THOI_GIAN_HIEN_TAI();
      NAP_THOI_GIAN_HTAI_VAO_DS13B07();
   }
   
  DOC_THOI_GIAN_TU_REALTIME();
  LICH_VIETNAM(NGAY_DS13,THANG_DS13,NAM_DS13);
  
   LAY_MA_8DOAN();
   
   WHILE(TRUE)
   {
      DOC_THOI_GIAN_TU_REALTIME();
      LICH_VIETNAM(NGAY_DS13,THANG_DS13,NAM_DS13);
      HIENTHI_THOI_GIAN();
      HIENTHI_NHIET_DO();
      
      FOR(K=0;K<10000;K++)       //KEO DAI THOI GIAN DOC NHIET DO.
      {
         PHIM_MOD();
         DELAY_US(50);
         PHIM_UP();
         DELAY_US(50);
         PHIM_DW();
         DELAY_US(50);
      }
      
      IF(GIATRI_MOD!=0)       NHAP_NHAY();
   }
}

