#INCLUDE <16F887.H>
#DEVICE ADC = 10
#FUSES NOWDT, PUT, HS, NOLVP, NOPROTECT
#USE DELAY(CLOCK=20M)

#INCLUDE <E:\Youtube\PIC16F887\THU_VIEN\TV_LCD_20X4.c>
#DEFINE BUZZER PIN_E2
#DEFINE DW     PIN_E1
#DEFINE UP     PIN_E0


UNSIGNED INT8 I,GH;
UNSIGNED INT16 KQ0,KQ1;

VOID HIENTHI()
{
      HIENTHI_FONT_LON(KQ0/10,0,5);
      HIENTHI_FONT_LON(KQ0%10,0,9);
      HIENTHI_FONT_LON(11,0,17);
      HIENTHI_FONT_LON(12,0,13);
     // LCD_DATA(0xDF);
    //  LCD_DATA(0x43);
      
      HIENTHI_FONT_LON(KQ1/10,2,5);
      HIENTHI_FONT_LON(KQ1%10,2,9);
      HIENTHI_FONT_LON(11,2,17);
      HIENTHI_FONT_LON(12,2,13);
      //LCD_DATA(0xDF);
     // LCD_DATA(0x43);
      
     // LCD_COMMAND(0XD2);
   //   LCD_DATA(GH/10 + 0X30);
   //   LCD_DATA(GH%10 + 0X30);
}

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
VOID DOC_LM350()
{
   SET_ADC_CHANNEL(0);
   KQ0=0;
      FOR(I=0;I<100;I++)
      {
         KQ0 = KQ0 + READ_ADC();
         DELAY_MS(1);
      }
      
      KQ0 = KQ0/2.046/100;
}

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
VOID DOC_LM351()
{
   SET_ADC_CHANNEL(1);
   KQ1=0;
      FOR(I=0;I<100;I++)
      {
         KQ1 = KQ1 + READ_ADC();
         DELAY_MS(1);
      }
      
      KQ1 = KQ1/2.046/100;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
VOID CD_UP()
{
   IF(!INPUT(UP))
   {
      DELAY_MS(20);
      IF(!INPUT(UP))
      {
         GH++;
         IF(GH>50)     GH = 50;
        // WHILE(!INPUT(UP));
      }
      DELAY_MS(500);
   }
}

VOID CD_DW()
{
   IF(!INPUT(DW))
   {
      DELAY_MS(20);
      IF(!INPUT(DW))
      {
         GH--;
         IF(GH<30)     GH = 30;
         // WHILE(!INPUT(DW));
      }
      DELAY_MS(500);
   }
}

VOID MAIN()
{
   SET_TRIS_D(0X00);
   SET_TRIS_B(0X00);
   SET_TRIS_A(0X03);
   GH=40;
   LCD_SETUP();
   
 //  LCD_COMMAND(0X80);
 //  LCD_DATA("DO ND 1:");
   
   //LCD_COMMAND(0XC0);
  // LCD_DATA("GIOI HAN TREN: ");
   
 //  LCD_COMMAND(0X94);
  // LCD_DATA("DO ND 2:");
  // LCD_COMMAND(0XD4);
  // LCD_DATA("DO ND LM35 1 :");
   
   SETUP_ADC(ADC_CLOCK_DIV_32);
   SETUP_ADC_PORTS(SAN0|SAN1);
   LAY_MA_8DOAN();
   
   WHILE(TRUE)
   {
      DOC_LM350();
      DOC_LM351();
    //  CD_UP();
    //  CD_DW();
    //  IF((KQ0|KQ1)>GH)     OUTPUT_HIGH(BUZZER);
   //   ELSE           OUTPUT_LOW(BUZZER);
      HIENTHI();
      
     
      
   }
}
