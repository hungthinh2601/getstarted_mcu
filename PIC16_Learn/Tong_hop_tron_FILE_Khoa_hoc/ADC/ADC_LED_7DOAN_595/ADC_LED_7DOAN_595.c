#INCLUDE <16F887.H>
#DEVICE ADC = 10
#FUSES NOWDT,HS,NOLVP,NOPROTECT,PUT
#USE DELAY(CLOCK=20M)

#INCLUDE <E:\Youtube\Pic16F887\THU_VIEN\tv_4led_7doan.c>

UNSIGNED INT8 I;
UNSIGNED INT16 KQ;

VOID MAIN()
{
   SET_TRIS_D(0X00);
   SET_TRIS_A(0X01);
   
   SETUP_ADC(ADC_CLOCK_DIV_32);        //khoi tao ADC
   SETUP_ADC_PORTS(SAN0);              //khoi tao ports
   SET_ADC_CHANNEL(0);                 //khoi tao kenh
   
   WHILE(TRUE)
   {
      KQ = 0;
      FOR(I=0;I<100;I++)
      {
         KQ = KQ + READ_ADC();         //doc gia tri ADC
         DELAY_MS(1);
      }
      KQ = KQ/100/2.046;
      XUAT_4LED_7DOAN_4SO(0XFF,0XFF,MA7DOAN[KQ/10],MA7DOAN[KQ%10]);
   }
}
