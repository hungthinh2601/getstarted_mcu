#INCLUDE <16F887.H>
#DEVICE  ADC = 10             //khai b�o adc
#FUSES NOWDT, PUT, HS, NOPROTECT, NOLVP
#USE DELAY(CLOCK=20M)

UNSIGNED INT8 I,CHUC;
UNSIGNED INT16 KQ;

CONST UNSIGNED CHAR MA7DOAN[16]= {0XC0,0XF9,0xA4,0XB0,0X99,0X92,0X82,0XF8,
0X80,0X90,0x88, 0x83, 0xC6, 0xA1, 0x86, 0x8E};

VOID MAIN()
{
   SET_TRIS_B(0X00);
   SET_TRIS_D(0X00);
   SET_TRIS_A(0X01);
   
   SETUP_ADC(ADC_CLOCK_DIV_32);     //khoi tao ADC
   SETUP_ADC_PORTS(SAN0);           //khoi tao Port ADC
   SET_ADC_CHANNEL(0);              //khoi tao kenh
   
   WHILE(TRUE)
   {
      KQ=0;
      FOR(I=0;I<100;I++)
      {
         KQ = KQ + READ_ADC();         //doc gia tri ADC
         DELAY_MS(1);
      }
      
      KQ = KQ/2.046;                //do chenh lech do phan giai cua LM35 voi do phan giai tinh duoc.
      KQ = KQ/100;
      CHUC=MA7DOAN[KQ/10];
      IF(CHUC==0XC0)    CHUC=0XFF;
      OUTPUT_B(MA7DOAN[KQ%10]);
      OUTPUT_D(CHUC);
   }
}


