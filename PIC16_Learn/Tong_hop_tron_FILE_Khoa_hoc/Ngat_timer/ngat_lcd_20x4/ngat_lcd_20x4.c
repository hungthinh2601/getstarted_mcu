#INCLUDE <16F887.H>
#FUSES NOWDT, PUT, HS, NOPROTECT, NOLVP
#USE DELAY(CLOCK=20M)
#INCLUDE <E:\Youtube\Pic16F887\THU_VIEN\TV_LCD_20X4.c>

#DEFINE  MOD  PIN_E0
#DEFINE  UP   PIN_E1
#DEFINE  DW   PIN_E2

unsigned INT8 gio,phut,giay,BDT,CGIO,DVGIO,CP,DVP,CGIAY,DVGIAY;
unsigned INT8 GIA_TRI;
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#int_timer1
VOID INTERRUPT_TIMER1()
{
   SET_TIMER1(3036);
   BDT++;
}
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
void hienthi()
{
      CGIO = GIO/10;
      DVGIO = GIO%10;
    
      CP = PHUT/10;
      DVP = PHUT%10;
      CGIAY=GIAY/10;
      DVGIAY=GIAY%10;
      lcd_command(0x80);
      lcd_data("HUYNH NHAT TUNG     ");
      HIENTHI_FONT_LON(CGIO, 1,0);
      HIENTHI_FONT_LON(DVGIO, 1,3);
//!   LCD_GOTO_XY(1,6);
//!   LCD_DATA(":");
//!   LCD_GOTO_XY(2,6);
//!   LCD_DATA(":");
      HIENTHI_FONT_LON(CP, 1,7);
      HIENTHI_FONT_LON(DVP, 1,10);
      LCD_GOTO_XY(1,13);
      LCD_DATA(":");
      LCD_GOTO_XY(2,13);
      LCD_DATA(":");
      HIENTHI_FONT_LON(CGIAY, 1,14);
      HIENTHI_FONT_LON(DVGIAY, 1,17);
      
    
   
}
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
VOID NHAP_NHAY()
{
   IF(GIA_TRI==1)       
   {
      LCD_GOTO_XY(1,0);
      CGIO=10;         
      LCD_GOTO_XY(1,3);     
      DVGIO=10;
      HIENTHI_FONT_LON(CGIO, 1,0);
      HIENTHI_FONT_LON(DVGIO, 1,3);
   }
   IF(GIA_TRI==2)       
   {
      LCD_GOTO_XY(1,7);       
      CP=10;           
      LCD_GOTO_XY(2,7);      
      DVP=10;  
      HIENTHI_FONT_LON(CP, 1,7);
      HIENTHI_FONT_LON(DVP, 1,10);
   }
   IF(GIA_TRI==3)       
   {
      LCD_GOTO_XY(1,14);      
      CGIAY = 10;   
      LCD_GOTO_XY(2,14);      
      DVGIAY=10; 
      HIENTHI_FONT_LON(CGIAY, 1,14);
      HIENTHI_FONT_LON(DVGIAY, 1,17);
   }
}
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
VOID PHIM_MOD()
{
   IF(!INPUT(MOD))
   {
      DELAY_MS(20);
      IF(!INPUT(MOD))
      {
         GIA_TRI++;
         IF(GIA_TRI==4)       GIA_TRI=0;
    //   WHILE(!INPUT(MOD));
      }
      WHILE(!INPUT(MOD));
   }
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
VOID PHIM_UP()
{
   IF(!INPUT(UP))
   {
      DELAY_MS(20);
      IF(!INPUT(UP))
      {
         SWITCH(GIA_TRI)
         {
            CASE 1:  IF(GIO==23)        GIO=0;
                     ELSE     GIO++;
                     BREAK;
            CASE 2:  IF(PHUT==59)       PHUT=0;
                     ELSE     PHUT++;
                     BREAK;
            CASE 3:  IF(GIAY==59)       GIAY=0;
                     ELSE     GIAY++;
                     BREAK;
                     
            DEFAULT: BREAK;
         }
         DELAY_MS(500);
      }
   //  WHILE(!INPUT(UP));
   }
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
VOID PHIM_DW()
{
   IF(!INPUT(DW))
   {
      DELAY_MS(20);
      IF(!INPUT(DW))
      {
         SWITCH(GIA_TRI)
         {
            CASE 1:  IF(GIO==0)        GIO=23;
                     ELSE     GIO--;
                     BREAK;
                     
            CASE 2:  IF(PHUT==0)       PHUT=59;
                     ELSE     PHUT--;
                     BREAK;
            
            CASE 3:  IF(GIAY==0)       GIAY=59;
                     ELSE     GIAY--;
                     BREAK;
         }
         DELAY_MS(500);
      }
    // WHILE(!INPUT(DW));
   }
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
VOID MAIN()
{
   SET_TRIS_D(0X00);
   lcd_setup();
   GIAY=0;
   PHUT=0;
   GIO=0;
   BDT=0;
   HIENTHI();
   SETUP_TIMER_1(T1_INTERNAL|T1_DIV_BY_8);
   SET_TIMER1(3036);
   
   ENABLE_INTERRUPTS(GLOBAL);
   ENABLE_INTERRUPTS(INT_TIMER1);
  
   LAY_MA_8DOAN();
   WHILE(TRUE)
   {
//!      IF(BDT>=4)
//!      {
//!         BDT=0;
//!            SAO++;
//!            IF(SAO==25)
//!            {  
//!              SAO=0;
//!               GIAY++; 
//!               IF(GIAY==60) 
//!               {
//!                  GIAY=0;
//!                  PHUT++;
//!                  IF(PHUT==60)
//!                     {
//!                        PHUT=0;
//!                        GIO++;
//!                        IF(GIO==24)       GIO=0;
//!                     }
//!               }
//!            }   
//!      
//!      
//!      ELSE
//!      {
//!         HIENTHI();
//!         PHIM_MOD();
//!         PHIM_UP();
//!         PHIM_DW();
//!      }
   
      IF(BDT<10)
      {
         
         HIENTHI();
         NHAP_NHAY();
         PHIM_MOD();
         PHIM_UP();
         PHIM_DW();
         
         IF(BDT<5)
         {
            
            LCD_GOTO_XY(1,6);
            LCD_DATA(":");
            LCD_GOTO_XY(2,6);
            LCD_DATA(":");
         }
         ELSE
         {
            LCD_GOTO_XY(1,6);
            LCD_DATA(" ");
            LCD_GOTO_XY(2,6);
            LCD_DATA(" ");
         }
         
      }
      
      ELSE
      {
            BDT=BDT-10;
               GIAY++; 
               IF(GIAY==60) 
               {
                  GIAY=0;
                  PHUT++;
                  IF(PHUT==60)
                     {
                        PHUT=0;
                        GIO++;
                        IF(GIO==24)       GIO=0;
                     }
               }
              
        }      
   }
}
